apt-get update

mkdir ~/symath

cd ~/symath

git clone https://github.com/facebookresearch/SymbolicMathematics.git

git clone https://github.com/NVIDIA/apex

apt-get install python3-pip

pip3 install torch==1.5.1+cu101 torchvision==0.6.1+cu101 -f https://download.pytorch.org/whl/torch_stable.html

cd apex

pip install -v --no-cache-dir ./

cd ~/symath/SymbolicMathematics

git clone https://Reemet11@bitbucket.org/Reemet11/symbolicmath.git

cd symbolicmath/generated/$1

mv ./* ../../..

cd ../../..

pip3 install numexpr

pip3 install sympy

python3 main.py --exp_name first_train --fp16 true --amp 2 --tasks "$1" --reload_data "$1,data.prefix.counts.train,data.prefix.counts.valid,data.prefix.counts.test" --reload_size 40000000 --emb_dim 1024 --n_enc_layers 6 --n_dec_layers 6 --n_heads 8 --optimizer "adam,lr=0.0001" --batch_size 32 --epoch_size 300000 --validation_metrics "valid_prim_$1_acc" 
