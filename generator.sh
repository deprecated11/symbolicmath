apt-get update

mkdir ~/symath

cd ~/symath

git clone https://github.com/facebookresearch/SymbolicMathematics.git

git clone https://github.com/NVIDIA/apex

apt-get install python3-pip

pip3 install torch==1.5.1+cu101 torchvision==0.6.1+cu101 -f https://download.pytorch.org/whl/torch_stable.html

cd apex

pip install -v --no-cache-dir ./

cd ~/symath/SymbolicMathematics


pip3 install numexpr

pip3 install sympy

python3 main.py  \
--export_data true \
--n_variables 1 \
--batch_size 32 \
--cpu false \
--exp_name prim_$1_data  \
--num_workers 20  \
--tasks $1 \
--env_base_seed -1 \
--n_coefficients 0 \
--leaf_probs "0.75,0,0.25,0" \
--max_ops 5 \
--max_int 5 \
--positive true \
--max_len 64 \
--operators "add:10,sub:3,mul:10,div:5,sqrt:4,pow2:4,pow3:2,pow4:1,pow5:1,ln:4,exp:4,sin:4,cos:4,tan:4,asin:1,acos:1,atan:1,sinh:1,cosh:1,tanh:1,asinh:1,acosh:1,atanh:1" \
