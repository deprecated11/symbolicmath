apt-get update

mkdir ~/symath

cd ~/symath

git clone https://github.com/facebookresearch/SymbolicMathematics.git

git clone https://github.com/NVIDIA/apex

apt-get install python3-pip

pip3 install torch==1.5.1+cu101 torchvision==0.6.1+cu101 -f https://download.pytorch.org/whl/torch_stable.html

cd apex

pip install -v --no-cache-dir ./

cd ~/symath/SymbolicMathematics

git clone https://Reemet11@bitbucket.org/Reemet11/symbolicmath.git

cd symbolicmath/data

mv -f evaluator.py ../../src/evaluator.py

mv ./* ../../

cd ../..

wget https://dl.fbaipublicfiles.com/SymbolicMathematics/models/$1.pth

pip3 install numexpr

pip3 install sympy

python3 main.py --batch_size 24 --emb_dim 1024 --n_enc_layers 6 --n_dec_layers 6 --n_heads 8 --exp_name $1_$2  --eval_only true --reload_model "$1.pth"   --tasks "prim_$2" --reload_data "prim_$2,prim_$2.train,prim_$2.valid,prim_$2.test" --beam_eval true  --beam_size 10 --eval_verbose 1
